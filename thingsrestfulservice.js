const express = require('express');
const parser = require('body-parser');

const app = express();
app.use(parser.urlencoded());
app.use(parser.json());

const PORT = process.env.port || 4201;

// post, not yet used, but kept as an example to guide coding later when there are posts
app.post('/post',
    (request, response) => {
        console.log('Posted.');
        response.send('request.address was' + request.address);
    }
);

// functions that define basic types: Room, Place, and Thing
function Room(name) {
    this.name = name;
}

function Place(name) {
    this.name = name;
}

function Thing(name, type) {
    this.name = name;
    this.type = type;
}

let rooms = [
    new Room('Living room'),
    new Room('Bedroom'),
    new Room('Kitchen')
];


let placesInLivingRoom = [
    new Place('Bookcase'),
    new Place('Coffee table'),
    new Place('Side table')
];
let placesInBedroom = [
    new Place('Bedstand'),
    new Place('Closet')
];
let placesInKitchen = [
    new Place('Counter'),
    new Place('Upper cabinets'),
    new Place('Lower cabinets')
];

let places = [
    placesInLivingRoom,
    placesInBedroom,
    placesInKitchen
];

let things = [
    // living room
    [
        [new Thing('M*A*S*H', 'Video'), new Thing('War And Peace', 'Book')],
        [new Thing('Glass art vase', 'Other'), new Thing('Picture album', 'Memento')],
        [new Thing('Box of CDs', 'Music')]
    ],
    // bedroom
    [
        [new Thing('Clock', 'Tool'), new Thing('Lamp', 'Tool')],
        [new Thing('Shirts', 'Clothing'),
            new Thing('Shorts', 'Clothing'),
            new Thing('Blankets', 'Other')
        ]
    ],
    // kitchen
    [
        [new Thing('Cleaning spray', 'Supply'),
            new Thing('Paper towels', 'Supply'),
            new Thing('Coffee maker', 'Tool')
        ],
        [new Thing('Canned soup', 'Food'),
            new Thing('Dried vegetables', 'Food'),
            new Thing('Cat food', 'Other')
        ],
        [new Thing('Pots', 'Cooking'),
            new Thing('Pans', 'Cooking'),
            new Thing('Silverware', 'Other'),
            new Thing('Cups', 'Other')
        ]
    ]
];

// root get: return the API requests possible
app.get('/',
    (request, response) => {
        let text = `
        <div style="font-size: 150%; font-family: sans-serif">
        API calls:
        </div>
        <div style="padding-left: 10%; font-size: 125%; font-family: sans-serif">
            <br/>
            get: /source/getRooms
            <br/>
            get: /source/getPlacesInRoom/room/[room name]
            <br/>
            get: /source/getThingsInPlaceInRoom/room/[room name]/place/[place name]
            <br/>
            post: /source/addThingToPlaceInRoom + { thing, place, room }
        </div>
        `;

        response.send(text);
    }
);

// /source/getRooms: return a list of all rooms as JSON objects
app.get('/source/getRooms',
    (request, response) => {
        response.send(rooms);
    }
);

// /source/getPlacesInRoom/room/[room]: return a list of all places in the provided room;
// the ':room' syntax means that a variable URL segment is treated as a parameter;
// this must be preceded by the parameter name ( 'room ') as a URL segment
app.get('/source/getPlacesInRoom/room/:room',
    (request, response) => {
        let room = request.params.room;

        let index = rooms.findIndex((x) => {
            return x.name == room;
        });

        response.send(places[index]);
    }
);

// /source/getThingsInPlaceInRoom/room/[room]/place/[place]: return all the things in the provided room and place
app.get('/source/getThingsInPlaceInRoom/room/:room/place/:place',
    (request, response) => {
        let room = request.params.room;
        let place = request.params.place;

        let roomPlaceThings = getThingsInPlaceInRoom(room, place);
        response.send(roomPlaceThings);
    }
);

function getThingsInPlaceInRoom(room, place) {
    let roomIndex = rooms.findIndex((x) => {
        return x.name == room;
    });

    let roomPlaces = places[roomIndex];
    let placeIndex = roomPlaces.findIndex((x) => {
        return x.name == place;
    });

    return things[roomIndex][placeIndex];
}

// /source/addThingToPlaceInRoom: add the provided thing to the things for the provided room and place;
// all args (thing, place, and room) are provided in the body
app.post('/source/addThingToPlaceInRoom',
    (request, response) => {
        let room = request.body.room;
        let place = request.body.place;
        let thing = request.body.thing;

        let roomPlaceThings = getThingsInPlaceInRoom(room.name, place.name);
        roomPlaceThings.push(thing);

        response.send(true);
    }
);


// run the service
app.listen(PORT, () => {
    console.log('ThingsRestfulService is now listening on port ' + PORT + '.');
});
